#!/bin/bash
#script  click_mouse
#Author: Domenico Caravaggio
# automatically run after user logon in
#
BASE_DIR='/home/domenico/bin/click_mouse'
CLICK_PID=$BASE_DIR'/click.pid'
FILE_LOG=$BASE_DIR'/log/click.log'
time=1
function loging {
        DATE=$( date )
        TEXT="${@}"
        echo "${DATE} - ${TEXT}" 1>&2
        echo "${DATE} - ${TEXT}" >> ${FILE_LOG}

}


function click_fun {
case $1 in
    "--kill"|"-k")
     if [[ ! -f "$CLICK_PID" ]]; then
        loging "not found pid"
        return
        else
        if ps -p $(cat $CLICK_PID ) > /dev/null
        then
            PID=$(cat $CLICK_PID )
            loging "$PID kill"            
            kill $PID &> /dev/null
            
        fi
        rm -f $CLICK_PID
       	loging "rm -f $CLICK_PID"
        fi
        
    ;;

    "-e"| "--exec")
        if [[  -f "$CLICK_PID" ]]; then
        if ps -p $(cat $CLICK_PID ) > /dev/null
        then
            PID=$(cat $CLICK_PID )
            loging "$PID  is running"
            loging "click running"
        return
        
        fi	
        fi
        if [[ "$2" == "-t"  && "$3" != "" ]] 
        then
            time=$3
        fi
    echo $$ > $CLICK_PID
    while true; xdotool click 1;do loging 'Press <CTRL+C> to exit.'; sleep $time; done
    ;;
    "--help" | "-h"|*)
        loging " click.sh : uso"
        loging " click.sh [ -e | --exec  | -k | --kill | -h | --help ]  [ -t ] time_sleep "
        loging " oppure ..."
        loging " CIAO ♥"
    ;;
esac

}

loging "######execute#####"
click_fun ${@}
loging "#################"
loging "######term#####"
exit 0
